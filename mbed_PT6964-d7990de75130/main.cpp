/* mbed PT6964 Test program, for PT6964 LED controller
 * Copyright (c) 2015, v01: WH, Initial version
 *               2016, v03: WH, updated Icon handling, UDCs and _putc()
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
#include "mbed.h"
#include "PT6964.h"

Serial pc(USBTX, USBRX);
DigitalOut myled(LED1);

// Select one of the testboards for Princeton PT6964 LED controller
#define PT6964_TEST   0 
#define DVD538A_TEST  1 

#if (DVD538A_TEST == 1)
//High level Control PT9664 for DVD538A

PT6964_DVD538A::DisplayData_t all_str  = {0xFF,0x0F, 0xFF,0x0F, 0xFF,0x0F, 0xFF,0x0F, 0xFF,0x0F, 0xFF,0x0F, 0xFF,0x0F};  
PT6964_DVD538A::DisplayData_t bye_str  = {LO(C7_B),HI(C7_B), LO(C7_Y),HI(C7_Y), LO(C7_E),HI(C7_E), 0x00,0x00, 0x00,0x00, 0x00,0x00, 0x00,0x00};

// KeyData_t size is 5 bytes  
PT6964_DVD538A::KeyData_t keydata; 

// PT6964 declaration, Default setting 7 Grids @ 10 Segments
PT6964_DVD538A PT6964_dvd538a(p5,p6,p7, p8);

char cmd;
int main() {
    
    pc.printf("Hello World\r\n"); //    
    
    PT6964_dvd538a.cls(); 
    PT6964_dvd538a.writeData(all_str);
    wait(4);
    PT6964_dvd538a.setBrightness(PT6964_BRT3);    
    wait(1);
    PT6964_dvd538a.setBrightness(PT6964_BRT0);        
    wait(1);
    PT6964_dvd538a.setBrightness(PT6964_BRT3);            
    
    while (1) {
     
      // Check and read keydata
      if (PT6964_dvd538a.getKeys(&keydata)) {
        pc.printf("Keydata 0..4 = 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x\r\n", keydata[0], keydata[1], keydata[2], keydata[3], keydata[4]);

        if (keydata[0] == 0x01) { //sw1 L/R  
          float delay = 0.2;

          PT6964_dvd538a.cls(); 
          PT6964_dvd538a.setIcon(PT6964_DVD538A::LD1); wait(delay);
          PT6964_dvd538a.setIcon(PT6964_DVD538A::LD2); wait(delay);
          PT6964_dvd538a.setIcon(PT6964_DVD538A::CD);  wait(delay);
          PT6964_dvd538a.setIcon(PT6964_DVD538A::DVD); wait(delay);
          PT6964_dvd538a.setIcon(PT6964_DVD538A::PSE); wait(delay);
          PT6964_dvd538a.setIcon(PT6964_DVD538A::PLY); wait(delay);
          PT6964_dvd538a.setIcon(PT6964_DVD538A::COL2); wait(delay);
          PT6964_dvd538a.setIcon(PT6964_DVD538A::MP4); wait(delay);
          PT6964_dvd538a.setIcon(PT6964_DVD538A::MP3); wait(delay);

          PT6964_dvd538a.clrIcon(PT6964_DVD538A::LD1); wait(delay);
          PT6964_dvd538a.clrIcon(PT6964_DVD538A::LD2); wait(delay);
          PT6964_dvd538a.clrIcon(PT6964_DVD538A::CD);  wait(delay);
          PT6964_dvd538a.clrIcon(PT6964_DVD538A::DVD); wait(delay);
          PT6964_dvd538a.clrIcon(PT6964_DVD538A::PSE); wait(delay);
          PT6964_dvd538a.clrIcon(PT6964_DVD538A::PLY); wait(delay);
          PT6964_dvd538a.clrIcon(PT6964_DVD538A::COL2); wait(delay);
          PT6964_dvd538a.clrIcon(PT6964_DVD538A::MP4); wait(delay);
          PT6964_dvd538a.clrIcon(PT6964_DVD538A::MP3); wait(delay);
        }       

        if (keydata[0] == 0x02) { //sw2 Stop  
          PT6964_dvd538a.cls(); 
        }  

        if (keydata[0] == 0x08) { //sw4 Play   
          PT6964_dvd538a.cls(); 
//          for (int i=0; i<16; i++) {
          for (int i=0x00; i<=0xFF; i++) {            
            PT6964_dvd538a.locate(0);            
            PT6964_dvd538a.printf("%04x", i);
//            PT6964_dvd538a.printf("%04X", i);            
            wait(0.2);
          }  
        }       

        if (keydata[0] == 0x10) { //sw3 Open  
#if(1)
//test to show all digits (base is 0x10)
          pc.printf("Show all hex digits\r\n");
          wait(1);          
          PT6964_dvd538a.cls(); 

          PT6964_dvd538a.printf("%04x", 0x01AB);
          cmd = pc.getc(); // wait for key                                     
          
          int val = 1; 
          for (int i=0; i<DVD538A_NR_DIGITS; i++) {

            for (int cnt=0; cnt<0x10; cnt++) {
              PT6964_dvd538a.locate(0);
              PT6964_dvd538a.printf("%04x", (val * cnt));

//              wait(0.5);
              cmd = pc.getc(); // wait for key
            }  
            val = val * 0x10;
          }
          pc.printf("\r\nShow all hex digits done\r\n");          
#endif 
        }  
        
      } //if

      myled = !myled;
      wait(0.3);      
    } //while
}
#endif

#if (PT6964_TEST == 1) 
//Direct control of PT9664 for DVD538

// DisplayData_t size is  8 bytes (4 digits @ 13 segments) OR 10 bytes (5 digits @ 12 segments) OR
//                       12 bytes (6 digits @ 11 segments) OR 14 bytes (7 digits @ 10 segments) 
PT6964::DisplayData_t mbed_str = {0xDA,0x00, 0x7C,0x00, 0x3C,0x01, 0xF6,0x00, 0x00,0x00, 0x00,0x00, 0x00,0x00};  
PT6964::DisplayData_t all_str  = {0xFF,0x0F, 0xFF,0x0F, 0xFF,0x0F, 0xFF,0x0F, 0xFF,0x0F, 0xFF,0x0F, 0xFF,0x0F};  
PT6964::DisplayData_t bye_str = {0x7C,0x00, 0xEC,0x00, 0x3C,0x01, 0x00,0x00, 0x00,0x00, 0x00,0x00, 0x00,0x00};  
PT6964::DisplayData_t hello_str = {0xDC,0x00, 0x3C,0x01, 0x38,0x00, 0x38,0x00, 0xF8,0x01, 0x00,0x00, 0x00,0x00};  
PT6964::DisplayData_t tst_str;  
PT6964::DisplayData_t hello1_str = {0x00,0x00, 0x00,0x00, 0x00,0x00, 0x00,0x00, 0x30,0x00, 0x00,0x00, 0x00,0x00};  
PT6964::DisplayData_t hello2_str = {0x00,0x00, 0x00,0x00, 0x00,0x00, 0xE8,0x02, 0x00,0x00, 0x00,0x00, 0x00,0x00};  
PT6964::DisplayData_t hello3_str = {0x00,0x00, 0x00,0x00, 0x78,0x02, 0x00,0x00, 0x00,0x00, 0x00,0x00, 0x00,0x00};  
PT6964::DisplayData_t hello4_str = {0x00,0x00, 0x30,0x03, 0x00,0x00, 0x00,0x00, 0x00,0x00, 0x00,0x00, 0x00,0x00};  


// KeyData_t size is 5 bytes  
PT6964::KeyData_t keydata; 

// PT6964 declaration, Default setting 7 Grids @ 10 Segments
PT6964 PT6964(p5,p6,p7, p8);

int main() {
    
    pc.printf("Hello World\r\n"); //    
    
    PT6964.cls(); 
    PT6964.writeData(all_str);
    wait(4);
    PT6964.setBrightness(PT6964_BRT3);    
    PT6964.writeData(mbed_str);    
    wait(1);
    PT6964.setBrightness(PT6964_BRT0);        
    wait(1);
    PT6964.setBrightness(PT6964_BRT3);            
    
    while (1) {
     
      // Check and read keydata
      if (PT6964.getKeys(&keydata)) {
        pc.printf("Keydata 0..4 = 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x\r\n", keydata[0], keydata[1], keydata[2], keydata[3], keydata[4]);

        if (keydata[0] == 0x01) { //sw1 L/R  
          PT6964.cls(); 
          PT6964.writeData(hello1_str);
                   
        }       

        if (keydata[0] == 0x02) { //sw2 Stop  
          PT6964.cls(); 
          PT6964.writeData(hello2_str);          

        }  

        if (keydata[0] == 0x08) { //sw4 Play   
          PT6964.cls(); 
          PT6964.writeData(hello3_str);          
         
        }       

        if (keydata[0] == 0x10) { //sw3 Open  
          PT6964.cls(); 
          PT6964.writeData(hello4_str);          

        }       
        
      } //if

      myled = !myled;
      wait(0.3);      
    } //while
}
#endif