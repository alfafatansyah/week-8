#include <stdio.h>

#define cmd_3 0xC0// COMMANDS 3: ADDRESS SETTING COMMANDS
#define clear_display 0x00
#define dig_1 0x00 // DIG 1, SG1-SG4 / SG5-SG8
#define dig_2 0x02 // DIG 2, SG1-SG4 / SG5-SG8
#define dig_3 0x04 // DIG 3, SG1-SG4 / SG5-SG8
#define dig_4 0x06 // DIG 4, SG1-SG4 / SG5-SG8
#define dig_5 0x08 // DIG 5, SG1-SG4 / SG5-SG8
#define dig_6 0x0A // DIG 6, SG1-SG4 / SG5-SG8
#define dig_7 0x0C // DIG 7, SG1-SG4 / SG5-SG8
#define dig_1_ext 0x01 // DIG 1, SG9-SG10 / SG13-SG14
#define dig_2_ext 0x03 // DIG 2, SG9-SG10 / SG13-SG14
#define dig_3_ext 0x05 // DIG 3, SG9-SG10 / SG13-SG14
#define dig_4_ext 0x07 // DIG 4, SG9-SG10 / SG13-SG14
#define dig_5_ext 0x09 // DIG 5, SG9-SG10 / SG13-SG14
#define dig_6_ext 0x0B // DIG 6, SG9-SG10 / SG13-SG14
#define dig_7_ext 0x0D // DIG 7, SG9-SG10 / SG13-SG14

int data_send;
int data_save = 0;

void reception (int x)
{
    int temp, final_temp;
	for(int i = 1; i <= 1 << 7; i = i << 1)
	{   
        final_temp <<= 1;
        temp = (x & i) ? 1 : 0;
        final_temp = (x & i) ? final_temp | 1 : final_temp | 0;
        printf("data x = %d, bit %d = %d\n",data_send, i, temp);
	}
    printf("data x = %d, temp = %d\n",data_send, final_temp);
}

int main(){
    
    data_send = cmd_3 | dig_7;
    reception(data_send);

    printf("data send = %d, data save = %d", data_send, data_save);

    return 0;
}