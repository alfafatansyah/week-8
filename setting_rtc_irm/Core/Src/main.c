/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <stdio.h>

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
RTC_HandleTypeDef hrtc;

TIM_HandleTypeDef htim1;
TIM_HandleTypeDef htim2;
TIM_HandleTypeDef htim16;

UART_HandleTypeDef huart2;

/* USER CODE BEGIN PV */
uint32_t tempcode, code, last_code;
uint32_t uart_buf_len;
uint8_t uart_buf[50];
uint8_t bitindex, cmdli, cmd, data_cmd_1, data_cmd_2, data_cmd_3, data_cmd_4, backup_hour, backup_minutes, edit_timeout, second_now, second_last;
uint8_t dig_sel[14] = {dig_1, dig_2, dig_3, dig_4, dig_5, dig_6, dig_7, dig_1_ext, dig_2_ext, dig_3_ext, dig_4_ext, dig_5_ext, dig_6_ext, dig_7_ext};
uint8_t pw_sel[8] = {pw_1_16, pw_2_16, pw_4_16, pw_10_16, pw_11_16, pw_12_16, pw_13_16, pw_14_16};
_Bool counting, display_status, edit_mode, display_edit, edit_select;

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_USART2_UART_Init(void);
static void MX_RTC_Init(void);
static void MX_TIM1_Init(void);
static void MX_TIM16_Init(void);
static void MX_TIM2_Init(void);
/* USER CODE BEGIN PFP */
void delay_us (uint16_t us);
void set_cmd_1 (uint8_t cmd_1_display);
void set_cmd_2 (uint8_t cmd_2_mode, uint8_t cmd_2_add, uint8_t cmd_2_wr);
void set_cmd_3 (uint8_t cmd_3_digit, uint8_t data);
void set_cmd_4 (uint8_t cmd_4_pulse, uint8_t cmd_4_display);
void reception (uint8_t cmd_set, uint8_t cmd_data); // Reception (Data / Command Write)
void print_segment(void);
uint8_t convert_character(uint8_t chara);
uint8_t convert_code (uint32_t code);
uint8_t convert_hex (uint8_t dec_value);
void set_time (uint8_t set_hour, uint8_t set_minutes);
void get_time(void);
void read_button(uint8_t state);
void initial_setting(void);
void clear_buff(void);

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_USART2_UART_Init();
  MX_RTC_Init();
  MX_TIM1_Init();
  MX_TIM16_Init();
  MX_TIM2_Init();
  /* USER CODE BEGIN 2 */
	HAL_TIM_Base_Start(&htim1);
	HAL_TIM_Base_Start(&htim2);
	HAL_TIM_Base_Start_IT(&htim16);
	HAL_UART_Receive_IT(&huart2, uart_buf, 5);
	__HAL_TIM_SET_COUNTER(&htim1, 0);
	//set_time();
	initial_setting();

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
		HAL_GPIO_TogglePin(GPIOC, GPIO_PIN_6);
		HAL_Delay(250);
		if(edit_mode == 1)
			display_edit = !display_edit;
		else
			display_edit = 0;
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Configure the main internal regulator output voltage
  */
  HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Configure LSE Drive Capability
  */
  HAL_PWR_EnableBkUpAccess();
  __HAL_RCC_LSEDRIVE_CONFIG(RCC_LSEDRIVE_LOW);
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI|RCC_OSCILLATORTYPE_LSE;
  RCC_OscInitStruct.LSEState = RCC_LSE_ON;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSIDiv = RCC_HSI_DIV1;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = RCC_PLLM_DIV1;
  RCC_OscInitStruct.PLL.PLLN = 8;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV2;
  RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the peripherals clocks
  */
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_RTC|RCC_PERIPHCLK_TIM1;
  PeriphClkInit.Tim1ClockSelection = RCC_TIM1CLKSOURCE_PCLK1;
  PeriphClkInit.RTCClockSelection = RCC_RTCCLKSOURCE_LSE;

  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief RTC Initialization Function
  * @param None
  * @retval None
  */
static void MX_RTC_Init(void)
{

  /* USER CODE BEGIN RTC_Init 0 */

  /* USER CODE END RTC_Init 0 */

  /* USER CODE BEGIN RTC_Init 1 */

  /* USER CODE END RTC_Init 1 */
  /** Initialize RTC Only
  */
  hrtc.Instance = RTC;
  hrtc.Init.HourFormat = RTC_HOURFORMAT_24;
  hrtc.Init.AsynchPrediv = 127;
  hrtc.Init.SynchPrediv = 255;
  hrtc.Init.OutPut = RTC_OUTPUT_DISABLE;
  hrtc.Init.OutPutRemap = RTC_OUTPUT_REMAP_NONE;
  hrtc.Init.OutPutPolarity = RTC_OUTPUT_POLARITY_HIGH;
  hrtc.Init.OutPutType = RTC_OUTPUT_TYPE_OPENDRAIN;
  hrtc.Init.OutPutPullUp = RTC_OUTPUT_PULLUP_NONE;
  if (HAL_RTC_Init(&hrtc) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN RTC_Init 2 */

  /* USER CODE END RTC_Init 2 */

}

/**
  * @brief TIM1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM1_Init(void)
{

  /* USER CODE BEGIN TIM1_Init 0 */

  /* USER CODE END TIM1_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM1_Init 1 */

  /* USER CODE END TIM1_Init 1 */
  htim1.Instance = TIM1;
  htim1.Init.Prescaler = 64-1;
  htim1.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim1.Init.Period = 0xffff-1;
  htim1.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim1.Init.RepetitionCounter = 0;
  htim1.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim1) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim1, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterOutputTrigger2 = TIM_TRGO2_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim1, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM1_Init 2 */

  /* USER CODE END TIM1_Init 2 */

}

/**
  * @brief TIM2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM2_Init(void)
{

  /* USER CODE BEGIN TIM2_Init 0 */

  /* USER CODE END TIM2_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM2_Init 1 */

  /* USER CODE END TIM2_Init 1 */
  htim2.Instance = TIM2;
  htim2.Init.Prescaler = 64-1;
  htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim2.Init.Period = 0xffff-1;
  htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim2.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim2) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim2, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM2_Init 2 */

  /* USER CODE END TIM2_Init 2 */

}

/**
  * @brief TIM16 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM16_Init(void)
{

  /* USER CODE BEGIN TIM16_Init 0 */

  /* USER CODE END TIM16_Init 0 */

  /* USER CODE BEGIN TIM16_Init 1 */

  /* USER CODE END TIM16_Init 1 */
  htim16.Instance = TIM16;
  htim16.Init.Prescaler = 64-1;
  htim16.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim16.Init.Period = 0xffff-1;
  htim16.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim16.Init.RepetitionCounter = 0;
  htim16.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim16) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM16_Init 2 */

  /* USER CODE END TIM16_Init 2 */

}

/**
  * @brief USART2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART2_UART_Init(void)
{

  /* USER CODE BEGIN USART2_Init 0 */

  /* USER CODE END USART2_Init 0 */

  /* USER CODE BEGIN USART2_Init 1 */

  /* USER CODE END USART2_Init 1 */
  huart2.Instance = USART2;
  huart2.Init.BaudRate = 115200;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  huart2.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart2.Init.ClockPrescaler = UART_PRESCALER_DIV1;
  huart2.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART2_Init 2 */

  /* USER CODE END USART2_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOF_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, DIO_Pin|CLK_Pin|STB_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(LD3_GPIO_Port, LD3_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin : T_NRST_Pin */
  GPIO_InitStruct.Pin = T_NRST_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(T_NRST_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : DIO_Pin CLK_Pin STB_Pin */
  GPIO_InitStruct.Pin = DIO_Pin|CLK_Pin|STB_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pin : IRM_Pin */
  GPIO_InitStruct.Pin = IRM_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(IRM_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : LD3_Pin */
  GPIO_InitStruct.Pin = LD3_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(LD3_GPIO_Port, &GPIO_InitStruct);

  /* EXTI interrupt init*/
  HAL_NVIC_SetPriority(EXTI4_15_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI4_15_IRQn);

}

/* USER CODE BEGIN 4 */
void delay_us (uint16_t us)
{
	__HAL_TIM_SET_COUNTER(&htim2,0);  // set the counter value a 0
	while (__HAL_TIM_GET_COUNTER(&htim2) < us);  // wait for the counter to reach the us input in the parameter
}
void set_cmd_1 (uint8_t cmd_1_display)
{
	data_cmd_1 = header_cmd_1 | cmd_1_display;
	reception(data_cmd_1, 0);
}

void set_cmd_2 (uint8_t cmd_2_mode, uint8_t cmd_2_add, uint8_t cmd_2_wr)
{
	data_cmd_2 = header_cmd_2 | cmd_2_mode | cmd_2_add | cmd_2_wr;
	reception(data_cmd_2, 0);
}

void set_cmd_3 (uint8_t cmd_3_digit, uint8_t data)
{
	data_cmd_3 = header_cmd_3 | cmd_3_digit;
	reception(data_cmd_3, data);
}

void set_cmd_4 (uint8_t cmd_4_pulse, uint8_t cmd_4_display)
{
	uint8_t temp;
	if(cmd_4_display == 1)
		temp = display_off;
	else
		temp = display_on;
	
	data_cmd_4 = header_cmd_4 | cmd_4_pulse | temp;
	reception(data_cmd_4, 0);
}

void reception (uint8_t cmd_set, uint8_t cmd_data) // Reception (Data / Command Write)
{	
	uint8_t temp;
	stb_low;	
	delay_us(1);
	
	for(int i = 1; i <= 1 << 7; i = i << 1)
	{
		temp = (cmd_set & i) ? 1 : 0;
		clk_low;
		HAL_GPIO_WritePin(DIO_GPIO_Port, DIO_Pin, temp);
		delay_us(1);
		clk_high;
		delay_us(1);
	}
	
	if(cmd_set >= header_cmd_3)
	{
		delay_us(1);
		for(int i = 1; i <= 1 << 7; i = i << 1)
		{
			temp = (cmd_data & i) ? 1 : 0;
			clk_low;
			HAL_GPIO_WritePin(DIO_GPIO_Port, DIO_Pin, temp);
			delay_us(1);
			clk_high;
			delay_us(1);
		}
	}
	stb_high;
	delay_us(1);
}

void print_segment(void)
{
	uint8_t s3, s2, s1, s0;
	
	if(edit_mode == 1)
	{
		s3 = backup_hour % 100 / 10;
		s2 = backup_hour % 10;
		s1 = backup_minutes % 100 / 10;
		s0 = backup_minutes % 10;
		
		set_cmd_2(data_write, inc_add, normal_mode);
		
		if(edit_select == 0)
		{
			if(display_edit == 0)
			{
				set_cmd_3(dig_2, convert_character(s3));
				set_cmd_3(dig_3, convert_character(s2));
			}
			else
			{
				set_cmd_3(dig_2, 0x00);
				set_cmd_3(dig_3, 0x00);
			}
			set_cmd_3(dig_4, convert_character(s1));	
			set_cmd_3(dig_5, convert_character(s0));			
		}		
		else
		{
			set_cmd_3(dig_2, convert_character(s3));
			set_cmd_3(dig_3, convert_character(s2));
			if(display_edit == 0)
			{
				set_cmd_3(dig_4, convert_character(s1));	
				set_cmd_3(dig_5, convert_character(s0));
			}	
			else
			{
				set_cmd_3(dig_4, 0x00);	
				set_cmd_3(dig_5, 0x00);
			}			
		}
		
		set_cmd_1(d7_s10);
		set_cmd_4(pw_14_16, 0);		
	}
	
	else
	{
		RTC_TimeTypeDef gtime;
		RTC_DateTypeDef gdate;
		HAL_RTC_GetTime(&hrtc, &gtime, RTC_FORMAT_BIN); 
		HAL_RTC_GetDate(&hrtc, &gdate, RTC_FORMAT_BIN);
		
		s3 = gtime.Hours % 100 / 10;
		s2 = gtime.Hours % 10;
		s1 = gtime.Minutes % 100 / 10;
		s0 = gtime.Minutes % 10;
		
		backup_hour = gtime.Hours;
		backup_minutes = gtime.Minutes;
		
		set_cmd_2(data_write, inc_add, normal_mode);
		
		set_cmd_3(dig_2, convert_character(s3));
		set_cmd_3(dig_4, convert_character(s1));	
		set_cmd_3(dig_5, convert_character(s0));
		if(gtime.Seconds % 2 == 0)
			set_cmd_3(dig_3, convert_character(s2) | dot_2_b);
		else
			set_cmd_3(dig_3, convert_character(s2));
	
		set_cmd_1(d7_s10);
		set_cmd_4(pw_14_16, 0);
	}	
}

uint8_t convert_character(uint8_t chara)
{
	switch (chara)
	{
		case 0:
        return n_0;
        break;
    case 1:
        return n_1;
        break;
    case 2:
        return n_2;
        break;
    case 3:
        return n_3;
        break;
    case 4:
        return n_4;
        break;
    case 5:
        return n_5;
        break;
    case 6:
        return n_6;
        break;
    case 7:
        return n_7;
        break;
    case 8:
        return n_8;
        break;
    case 9:
        return n_9;
        break;
   }
}

uint8_t convert_code (uint32_t code)
{
	switch (code)
	{
		case (0x8976E817):	// Power button
			return 21;
			break;
		
		case (0x897650AF):	// Source button
			return 22;
			break;
		
		case (0x89761AE5):	// Bluetooth button
			return 23;
			break;
		
		case (0x8976807F):	// 1 button
			return 1;
			break;			

		case (0x897640BF):	// 2 button
			return 2;
			break;

		case (0x8976C03F):	// 3 button
			return 3;
			break;

		case (0x897620DF):	// 4 button
			return 4;
			break;

		case (0x8976A05F):	// 5 button
			return 5;
			break;

		case (0x8976609F):	// 6 button
			return 6;
			break;

		case (0x8976E01F):	// 7 button
			return 7;
			break;

		case (0x897610EF):	// 8 button
			return 8;
			break;
			
		case (0x8976906F):	// 9 button
			return 9;
			break;		
				
		case (0x897600FF):	// 0 button
			return 10;
			break;
			
		case (0x8976C837):	// Play / Pause button
			return 11;
			break;
		
		case (0x89768A75):	// Vol - button
			return 12;
			break;
			
		case (0x89760AF5):	// Vol + button
			return 13;
			break;
		
		case (0x8976D827):	// Prev button
			return 14;
			break;
		
		case (0x897658A7):	// Next button
			return 15;
			break;		
		
		case (0x897616E9):	// Scan button
			return 16;
			break;
		
		case (0x89768877):	// Memory button
			return 17;
			break;
		
		case (0x89763CC3):	// Light button
			return 18;
			break;
		
		case (0x8976A857):	// Mute button
			return 19;
			break;
		
		case (0x89767887):	// Repeat button
			return 24;
			break;
		
		case (0x89764AB5):	// Karaoke button
			return 25;
			break;
		
		case (0x89765CA3):	// Bass button
			return 26;
			break;
		
		case (0x8976BC43):	// Middle button
			return 27;
			break;
		
		case (0x89761CE3):	// Treble button
			return 88;
			break;
		
		case (0x8976BA45):	// Super Bass button
			return 29;
			break;
		
		case (0x89768C73):	// Eq button
			return 30;
			break;
		
		case (0x89760CF3):	// Bazzoke button
			return 31;
			break;
		
		default:
      return NULL;
	}
}

uint8_t convert_hex (uint8_t dec_value)
{
	uint8_t hex_byte, low_byte;
	hex_byte = dec_value % 100 / 10;
	hex_byte <<= 4;
	low_byte = dec_value % 10;	
	hex_byte = hex_byte | low_byte;	
	
	return hex_byte;
}

void set_time (uint8_t set_hour, uint8_t set_minutes)
{
	RTC_TimeTypeDef stime; 
  RTC_DateTypeDef sdate; 
  stime.Hours = convert_hex(set_hour); // set hours 
  stime.Minutes = convert_hex(set_minutes); // set minutes 
  stime.Seconds = 0x00; // set seconds 
  stime.DayLightSaving = RTC_DAYLIGHTSAVING_NONE; 
  stime.StoreOperation = RTC_STOREOPERATION_RESET; 
	if (HAL_RTC_SetTime(&hrtc, &stime, RTC_FORMAT_BCD) != HAL_OK) 
  { 
    //_Error_Handler(__FILE__, __LINE__); 
  }
  sdate.WeekDay = RTC_WEEKDAY_TUESDAY; // day 
  sdate.Month = RTC_MONTH_DECEMBER; // month 
  sdate.Date = 0x1B; // date 
  sdate.Year = 0x16; // year 
	if (HAL_RTC_SetDate(&hrtc, &sdate, RTC_FORMAT_BCD) != HAL_OK) 
  { 
    //_Error_Handler(__FILE__, __LINE__); 
  } 
  HAL_RTCEx_BKUPWrite(&hrtc, RTC_BKP_DR1, 0x32F2); // backup register 
	
	RTC_TimeTypeDef gtime;
	RTC_DateTypeDef gdate;
	HAL_RTC_GetTime(&hrtc, &gtime, RTC_FORMAT_BIN); 
	HAL_RTC_GetDate(&hrtc, &gdate, RTC_FORMAT_BIN);
	
	uart_buf_len = sprintf(uart_buf, "Time Update >> %02d:%02d\r\n", gtime.Hours, gtime.Minutes);
	HAL_UART_Transmit(&huart2, (uint8_t *)uart_buf, uart_buf_len, 100);
}

void get_time(void) 
{
	RTC_TimeTypeDef gtime; 
	RTC_DateTypeDef gdate; 
	HAL_RTC_GetTime(&hrtc, &gtime, RTC_FORMAT_BIN); 
	HAL_RTC_GetDate(&hrtc, &gdate, RTC_FORMAT_BIN); 
}

void read_button(uint8_t state)
{
	if(state == 17) // Play / Pause
		edit_mode = !edit_mode;
	
	if(state == 15 && edit_select < 1)
		edit_select++;
	
	if(state == 14 && edit_select > 0)
		edit_select--;
	
	if(state == 13 && edit_mode == 1)
	{
		if(edit_select == 0)
		{
			backup_hour++;
			if(backup_hour > 23)
				backup_hour = 0;
		}
		else
		{
			backup_minutes++;	
			if(backup_minutes > 59)
				backup_minutes = 0;
		}			
	}
	
	if(state == 12 && edit_mode == 1)
	{
		if(edit_select == 0)
		{
			backup_hour--;
			if(backup_hour > 23)
				backup_hour = 23;
		}
		else
		{
			backup_minutes--;	
			if(backup_minutes > 59)
				backup_minutes = 59;			
		}
	}
	
	if(state == 11 && edit_mode == 1)
	{
		edit_mode = 0;
		set_time(backup_hour, backup_minutes);
		clear_buff();
	}
	
	if(edit_mode ==0)
	{
		edit_select = 0;
		edit_timeout = 0
	}
	
	else if(edit_mode ==1)
	{
		if(second_now != second_last)
		{
			edit_timeout++;
			
		}
	}
	
	code = NULL;
}

void initial_setting(void)
{
	HAL_Delay(200);
	set_cmd_2(data_write, 0, 0);
	for(int i = 0; i < 14; i++)
	{
		set_cmd_3(dig_sel[i], 0);
	}		
	set_cmd_1(d7_s10);
	for(int i = 0; i < 8; i++)
	{
		set_cmd_4(pw_sel[i], 1);
	}		
	set_cmd_1(d7_s10);
	for(int i = 0; i < 8; i++)
	{
		set_cmd_4(pw_sel[i], 0);
	}		
}

void clear_buff(void)
{
	for(int i = 0; i < 50; i ++)
			uart_buf[i] = 0;
}

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{	
	if (code != 0)
			last_code = code;
	
	read_button(convert_code(code));
	print_segment();
}
void HAL_GPIO_EXTI_Falling_Callback(uint16_t GPIO_Pin)
{
	if(GPIO_Pin == IRM_Pin)
	{
		if(__HAL_TIM_GET_COUNTER(&htim1) > 8000)
		{
			tempcode = 0;
			bitindex = 0;
		}
		else if(__HAL_TIM_GET_COUNTER(&htim1) > 1700)
		{
			tempcode |= (1UL << (31-bitindex));	// write 1
			bitindex++;
		}
		else if(__HAL_TIM_GET_COUNTER(&htim1) > 1000)
		{
			tempcode &= ~(1UL << (31-bitindex));	// write 0
			bitindex++;
		}		
		if(bitindex == 32)
		{
			cmdli = ~tempcode;	// logical inverted last 8 bits
			cmd = tempcode >> 8;	// second last 8 bits
			if(cmdli == cmd)	// check for errors
			{
				code = tempcode;	// if no bit errors				
			}
			bitindex = 0;
		}
			__HAL_TIM_SET_COUNTER(&htim1, 0);
	}
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart) 
{
  HAL_UART_Receive_IT(&huart2, uart_buf, 5); 
	
	if(uart_buf[2] == 0x3A && edit_mode == 0)
	{				
		uart_buf[0] = uart_buf[0] << 4;
		uart_buf[0] = uart_buf[0] >> 4;	
		uart_buf[1] = uart_buf[1] << 4;
		uart_buf[1] = uart_buf[1] >> 4;	
		uart_buf[3] = uart_buf[3] << 4;
		uart_buf[3] = uart_buf[3] >> 4;
		uart_buf[4] = uart_buf[4] << 4;
		uart_buf[4] = uart_buf[4] >> 4;			
		
		backup_hour = uart_buf[0] * 10 + uart_buf[1];
		backup_minutes = uart_buf[3] * 10 + uart_buf[4];
		
		if(backup_hour < 24 && backup_minutes < 60)
		{
			set_time(backup_hour, backup_minutes);			
		}
		else
		{
			uart_buf_len = sprintf(uart_buf, "Time Update >> Invalid\r\n");
			HAL_UART_Transmit(&huart2, (uint8_t *)uart_buf, uart_buf_len, 100);
		}		
	}
	
	else
	{
		uart_buf_len = sprintf(uart_buf, "Time Format >> Invalid\r\n");
		HAL_UART_Transmit(&huart2, (uint8_t *)uart_buf, uart_buf_len, 100);
	}
	
	clear_buff();
}

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
